class Level3D {

    constructor(levelLoaded) {
        console.log("Level3D.js działa")

        function isOdd(num) { return num % 2; }

        var container = new THREE.Object3D() // kontener na obiekty 3D
        for (var k = 0; k < levelLoaded.length; k++) {
            if (isOdd(levelLoaded[k].x) == 0) {
                const x = (2 * radius / Math.sqrt(3)) * (levelLoaded[k].x * 1.5)
                const z = ((settings.radius * 2 * levelLoaded[k].z))
                let hex1 = new Hex3D(levelLoaded, k, x, z)
                hex1.position.x = x
                hex1.position.z = z
                container.add(hex1)
                if (levelLoaded[k].type == "light") {
                    const light = new Light(x, z)
                    container.add(light)
                    light.lookAt(container.position)
                }
                else if (levelLoaded[k].type == "treasure") {
                    const treasure = new Treasure3D(x, z)
                    container.add(treasure)
                }
                else if (levelLoaded[k].type == "enemy") {
                    const enemy = new Enemy3D(x, z)
                    container.add(enemy)
                }
                else if (levelLoaded[k].type == "ally") {
                    let allyModelLevel = new AllyModel3D()
                    settings.createdAllies.push(allyModelLevel)
                    settings.isMovingAlly.push(false)
                    // console.log("LOADMODEL00000")
                    allyModelLevel.loadModel("js3D/player/models/allyModel3D.js", function (modeldata) {
                        // console.log("LOADMODEL55555")
                        console.log("model ally został załadowany")//, x, z)//, modeldata)
                        settings.intersectObjects.push(allyModelLevel.container)
                        container.add(allyModelLevel.container)
                        allyModelLevel.container.position.x = x
                        allyModelLevel.container.position.z = z
                    })
                }
                if (k == 0) { settings.playerStartPos.push(x, z) } //start pos gracza
            }
            else if (isOdd(levelLoaded[k].x) == 1) {
                const x = (2 * radius / Math.sqrt(3)) * (levelLoaded[k].x * 1.5)
                const z = ((settings.radius * 2 * levelLoaded[k].z) + settings.radius)
                let hex1 = new Hex3D(levelLoaded, k, x, z)
                hex1.position.x = x
                hex1.position.z = z
                container.add(hex1)
                if (levelLoaded[k].type == "light") {
                    const light = new Light(x, z)
                    container.add(light)
                    light.lookAt(container.position);
                }
                else if (levelLoaded[k].type == "treasure") {
                    const treasure = new Treasure3D(x, z)
                    container.add(treasure)
                }
                else if (levelLoaded[k].type == "enemy") {
                    const enemy = new Enemy3D(x, z)
                    container.add(enemy)
                }
                else if (levelLoaded[k].type == "ally") {
                    let allyModelLevel = new AllyModel3D()
                    settings.createdAllies.push(allyModelLevel)
                    settings.isMovingAlly.push(false)
                    // console.log("LOADMODEL00000")
                    allyModelLevel.loadModel("js3D/player/models/allyModel3D.js", function (modeldata) {
                        // console.log("LOADMODEL55555")
                        console.log("model ally został załadowany")//, x, z)//, modeldata)
                        settings.intersectObjects.push(allyModelLevel.container)
                        container.add(allyModelLevel.container)
                        allyModelLevel.container.position.x = x
                        allyModelLevel.container.position.z = z
                    })
                }
                if (k == 0) { settings.playerStartPos.push(x, z) } //start pos gracza
            }
        }
        return container
    }
}