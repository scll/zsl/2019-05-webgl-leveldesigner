class Light {

    constructor(x, z) {
        console.log("Light.js działa")
        this.container = new THREE.Object3D() // kontener na obiekty 3D

        const { lightHeight } = settings
        this.light = new THREE.SpotLight(0xffffff, 2.5, 600, (Math.PI), 1);
        this.light.position.set(x, lightHeight, z);
        this.light.castShadow = true
        this.container.add(this.light)
        settings.savedLights.push(this.light) //dodanie obiektu do pozniejszej zmiany za pomoca range inputow

        this.lampGeometry = new THREE.OctahedronGeometry(10, 0);
        this.lampMaterial = new THREE.MeshBasicMaterial({
            transparent: false,
            wireframe: true,
            color: 0xff0000,
            side: THREE.DoubleSide,
        });
        this.lamp = new THREE.Mesh(this.lampGeometry, this.lampMaterial)
        this.lamp.position.set(x, lightHeight, z);
        this.container.add(this.lamp);
        settings.savedLightLamps.push(this.lamp) //dodanie obiektu do pozniejszej zmiany za pomoca range inputow

        return this.container
    }
}