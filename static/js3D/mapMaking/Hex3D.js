class Hex3D {

    constructor(levelLoaded, k, x, z) { //k - id tworzonego hexagonu

        var dirOut = levelLoaded[k].dirOut
        var dirIn;
        if (levelLoaded[k - 1]) {
            parseInt(levelLoaded[k - 1].dirOut) - 3 < 0 ? dirIn = parseInt(levelLoaded[k - 1].dirOut) + 3 : dirIn = parseInt(levelLoaded[k - 1].dirOut) - 3
        };

        var container = new THREE.Object3D() // kontener na obiekty 3D
        var floorGeo = new THREE.CylinderGeometry(settings.dlugoscBoku, settings.dlugoscBoku, 0, 6, 1, false, 0.5)
        var floor = new THREE.Mesh(floorGeo, settings.wallMaterialDefault)
        floor.name = "floor"
        floor.position.y = -1 * settings.wysokoscBoku / 2
        // floor.receiveShadow = true
        settings.intersectObjects.push(floor)

        var wall = new THREE.Mesh(settings.wallGeometryDefault, settings.wallMaterialDefault);
        for (var n = -5; n <= 0; n++) {
            if (n == -dirOut) {
                var side = new Doors3D()
            }
            else if (n == -dirIn) {
                var side = new Doors3D()
            }
            else {
                var side = wall.clone()
            }
            side.position.z = settings.radius * Math.cos(Math.PI / 3 * (n - 3))
            side.position.x = settings.radius * Math.sin(Math.PI / 3 * (n - 3))
            side.lookAt(container.position)
            // side.castShadow = true
            container.add(side)
            container.add(floor)
            // container.castShadow = true
        }
        return container

    }
}