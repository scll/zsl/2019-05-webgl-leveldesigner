class Treasure3D {

    constructor(x, z) {
        console.log("Treasure3D.js działa")
        this.treasureGeometry = new THREE.SphereGeometry(30, 10, 10, 0, Math.PI * 2, 0, 0.5 * Math.PI);
        this.treasureMaterial = new THREE.MeshBasicMaterial({
            transparent: false,
            wireframe: true,
            color: 0xaa5500,
            side: THREE.DoubleSide,
        });
        this.treasure = new THREE.Mesh(this.treasureGeometry, this.treasureMaterial)
        this.treasure.name = "ally"
        if (x != undefined) this.treasure.position.set(x, 10, z);

        settings.intersectObjects.push(this.treasure)
        return this.treasure
    }
}