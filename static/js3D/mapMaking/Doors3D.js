class Doors3D {

    constructor() {
        // console.log("Doors3D.js działa")

        const container = new THREE.Object3D() // kontener na obiekty 3D
        const wall1 = new THREE.Mesh(settings.wallGeometryDoors, settings.wallMaterialDefault);
        wall1.position.x = (2 * settings.radius) / (Math.sqrt(3) * 3);
        const wall2 = new THREE.Mesh(settings.wallGeometryDoors, settings.wallMaterialDefault);
        wall2.position.x = (2 * settings.radius) / (Math.sqrt(3) * -3);
        container.add(wall1)
        container.add(wall2)

        return container
    }

}