class Enemy3D {

    constructor(x, z) {
        console.log("Enemy3D.js działa")
        this.enemyGeometry = new THREE.BoxGeometry(55, 55, 55, 5, 5);
        this.enemyMaterial = new THREE.MeshBasicMaterial({
            transparent: false,
            wireframe: true,
            color: 0xff0000,
            side: THREE.DoubleSide,
        });
        this.enemy = new THREE.Mesh(this.enemyGeometry, this.enemyMaterial)
        if (x != undefined) { this.enemy.position.set(x, 3, z); }

        return this.enemy
    }
}