class testMovement3DAllies {

    constructor() {

        const scene = new THREE.Scene();
        const windowWidth = $(window).width()
        const windowHeight = $(window).height()
        const camera = new THREE.PerspectiveCamera(
            45,    // kąt patrzenia kamery (FOV)
            windowWidth / windowHeight,    // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
            0.1,    // minimalna renderowana odległość
            10000    // maxymalna renderowana odległość od kamery
        );
        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor(0xffffff);
        renderer.setSize(windowWidth, windowHeight);
        $("#root").append(renderer.domElement);

        camera.position.set(0, 700, 700)
        camera.lookAt(scene.position)
        const axes = new THREE.AxesHelper(1000)
        scene.add(axes)

        var container = new THREE.Object3D() // kontener na obiekty 3D
        var floorGeo = new THREE.BoxGeometry(1000, 1, 1000, 30, 30, 30)
        var floorMaterial = new THREE.MeshBasicMaterial({
            transparent: false,
            wireframe: true,
            color: 0x000000,
            side: THREE.DoubleSide
        })
        var floor = new THREE.Mesh(floorGeo, floorMaterial)
        floor.name = "floor"
        container.add(floor)
        obj.intersObjects.push(floor)
        scene.add(container)

        const container2 = new THREE.Object3D()
        const player = new Enemy3D();
        const { wysokoscBoku } = settings
        player.position.y = wysokoscBoku / 2 + 2
        container2.add(player)

        const axis2 = new THREE.AxesHelper(200)
        container2.add(axis2)
        scene.add(container2)

        const ally = new Treasure3D()
        for (let i = 0; i < 5; i++) {
            let allyClone = ally.clone()
            allyClone.name = "ally"
            let x = Math.floor(Math.random() * 1000) - 500
            let z = Math.floor(Math.random() * 1000) - 500
            allyClone.position.set(x, 2, z)
            scene.add(allyClone)
            obj.intersObjects.push(allyClone)
        }

        $(document).mousedown(function () {
            move(event)
            $(document).on("mousemove", function (event) {
                move(event)
            })
            $(document).on("mouseup", function (event) {
                $(document).off("mousemove")
            })
        })
        function move(event) {
            obj.mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
            obj.mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
            obj.raycaster.setFromCamera(obj.mouseVector, camera);
            var intersects = obj.raycaster.intersectObjects(obj.intersObjects, true); //true - mozliwosc "klikania" dzieci dzieci sceny
            // if()
            if (intersects.length > 0) {
                obj.lastClickedName = intersects[0].object.name
                if (intersects[0].object.name == "floor") {
                    //////////////////////
                    obj.clickedVect = intersects[0].point
                    // console.log(obj.clickedVect)
                    obj.directionVect = obj.clickedVect.clone().sub(container2.position).normalize()
                    var angle = Math.atan2(
                        container2.position.clone().x - obj.clickedVect.x,
                        container2.position.clone().z - obj.clickedVect.z
                    )
                    player.rotation.y = angle - Math.PI
                    axis2.rotation.y = angle - Math.PI
                    //////////////////////
                }
                if (intersects[0].object.name == "ally") {
                    console.log("ALLY")
                    if (!obj.choosenAllies.includes(intersects[0].object)) {
                        obj.choosenAllies.push(intersects[0].object)
                        obj.allyVectors.push(new THREE.Vector3(0, 0, 0))
                        obj.allyDirectionVectors.push(new THREE.Vector3(0, 0, 0))
                    }
                }
                // console.log(container2.clone().position)
                // console.log(container2.position.clone())
                for (let i = 0; i < obj.choosenAllies.length; i++) {
                    obj.allyVectors[i] = container2.clone().position
                    obj.allyDirectionVectors[i] = obj.allyVectors[i].clone().sub(obj.choosenAllies[i].position).normalize()
                    // let angle = Math.atan2(
                    //     obj.choosenAllies[i].position.clone().x - obj.allyVectors[i].x,
                    //     obj.choosenAllies[i].position.clone().z - obj.allyVectors[i].z
                    // )
                    // obj.choosenAllies[i].rotation.y = angle - Math.PI
                }
            }
        }
        function render() {
            var distanceTo = container2.position.clone().distanceTo(obj.clickedVect)
            if (distanceTo > 5) {
                container2.translateOnAxis(obj.directionVect, 5)
            }
            for (let i = 0; i < obj.choosenAllies.length; i++) {

                obj.allyVectors[i] = container2.clone().position
                obj.allyDirectionVectors[i] = obj.allyVectors[i].clone().sub(obj.choosenAllies[i].position).normalize()

                var distanceTo2 = obj.choosenAllies[i].position.clone().distanceTo(obj.allyVectors[i])
                if (distanceTo2 > (70 * i + 70)) {
                    obj.choosenAllies[i].translateOnAxis(obj.allyDirectionVectors[i], 5)
                }

            }

            // for (let i = 0; i < obj.choosenAllies.length; i++) {
            //     if (i == 0) {
            //         // let clickedVect = container2.clone().position
            //         // let directionVect = clickedVect.clone().sub(currentAlly.position).normalize()
            //         // let angle = Math.atan2(
            //         //     currentAlly.position.clone().x - clickedVect.x,
            //         //     currentAlly.position.clone().z - clickedVect.z
            //         // )
            //         // currentAlly.rotation.y = angle - Math.PI

            //         // let distanceTo = currentAlly.position.clone().distanceTo(clickedVect)
            //         // if (distanceTo > 155) {
            //         //     currentAlly.translateOnAxis(directionVect, 5)
            //         // }
            //         let directionVect22 = container2.position.clone().sub(obj.choosenAllies[i].position).normalize()
            //         let distanceAlly = obj.choosenAllies[i].position.clone().distanceTo(container2.clone().position)
            //         // console.log(container2.position)
            //         // console.log(obj.savedPlayerPositions[20])
            //         if (distanceAlly > 120) {
            //             // console.log(directionVect22)
            //             obj.choosenAllies[i].lookAt(container2.position)
            //             obj.choosenAllies[i].translateOnAxis(directionVect22, 5)
            //             var angle = Math.atan2(
            //                 obj.choosenAllies[i].position.clone().x - container2.position.clone().x,
            //                 obj.choosenAllies[i].position.clone().z - container2.position.clone().z
            //             )
            //             obj.choosenAllies[i].rotation.y = angle - Math.PI
            //         }

            //     }
            //     if (i >= 1) {
            //         obj.choosenAllies[i].lookAt(obj.choosenAllies[i - 1].position)
            //         let directionVect22 = obj.choosenAllies[i - 1].position.position.clone().sub(obj.choosenAllies[i].position).normalize()
            //         let distanceAlly = obj.choosenAllies[i].position.clone().distanceTo(obj.choosenAllies[i - 1].position.clone().position)
            //         // console.log(container2.position)
            //         // console.log(obj.savedPlayerPositions[20])
            //         if (distanceAlly > 120) {
            //             // console.log(directionVect22)
            //             obj.choosenAllies[i].lookAt(obj.choosenAllies[i - 1].position)
            //             obj.choosenAllies[i].translateOnAxis(directionVect22, 5)
            //             var angle = Math.atan2(
            //                 obj.choosenAllies[i].position.clone().x - obj.choosenAllies[i - 1].position.clone().x,
            //                 obj.choosenAllies[i].position.clone().z - obj.choosenAllies[i - 1].position.clone().z
            //             )
            //             obj.choosenAllies[i].rotation.y = angle - Math.PI
            //         }
            //     }
            // }


            camera.position.x = container2.position.x
            camera.position.z = container2.position.z + 600
            camera.position.y = container2.position.y + 600
            camera.lookAt(container2.position)
            requestAnimationFrame(render);
            renderer.render(scene, camera);
        }
        render();
    }
}