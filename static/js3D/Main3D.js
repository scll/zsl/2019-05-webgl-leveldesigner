class Main3D {

    constructor() {
        console.log("Main3D.js działa")
        const that = this
        this.scene = new THREE.Scene();
        const windowWidth = $(window).width()
        const windowHeight = $(window).height()

        this.camera = new THREE.PerspectiveCamera(
            45,    // kąt patrzenia kamery (FOV)
            windowWidth / windowHeight,    // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
            0.1,    // minimalna renderowana odległość
            10000    // maxymalna renderowana odległość od kamery
        );
        const renderer = new THREE.WebGLRenderer({ antialias: true });
        // renderer.shadowMap.enabled = true
        // renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        renderer.setClearColor(0xffffff);
        renderer.setSize(windowWidth, windowHeight);
        $("#root").append(renderer.domElement);

        this.camera.position.set(0, 1000, 1000)
        const axes = new THREE.AxesHelper(1000)
        this.scene.add(axes)

        $.ajax({
            url: "/loadHex3D",
            data: "cokolwiek",
            type: "POST",
            success: function (data) {
                const obj = JSON.parse(data)
                console.log(obj)
                const levelLoaded = obj.level
                const lastHexSaved = levelLoaded[levelLoaded.length - 1]

                if (document.getElementById("level")) {
                    const level = new Level3D(levelLoaded);
                    main.scene.add(level)
                }
                if (document.getElementById("hex")) {
                    levelLoaded = [lastHexSaved];
                    const k = 0
                    const hex = new Hex3D(levelLoaded, k);
                    main.scene.add(hex)
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });


        playerModel.loadModel("js3D/player/models/playerModel3D.js", function (modeldata) {
            console.log("model gracza został załadowany")//, modeldata)
            settings.loadedModels++
            that.movement = new Movement(modeldata);
            that.playerCont = that.movement.getPlayerCont()
            that.playerCont.position.x = settings.playerStartPos[0]
            that.playerCont.position.z = settings.playerStartPos[1]
            that.scene.add(that.playerCont)
            if (settings.loadedModels == 1) {
                render();
            }
        })
        this.allyCont = 6
        // allyModel.loadModel("js3D/player/models/allyModel3D.js", function (modeldata) {
        //     settings.loadedModels++
        //     console.log("model ally został załadowany")//, modeldata)
        //     that.allyCont = modeldata
        //     settings.intersectObjects.push(that.allyCont)
        //     that.scene.add(that.allyCont)
        //     // render();
        //     if (settings.loadedModels == 2) {
        //         render();
        //     }
        // })

        // const orbitControl = new THREE.OrbitControls(this.camera, renderer.domElement);
        // orbitControl.addEventListener('change', function () {
        //     renderer.render(that.scene, that.camera)
        // });

        var isMovingPlayer = true
        const clock = new THREE.Clock();
        function render() {
            var delta = clock.getDelta();
            if (playerModel.mixer) playerModel.mixer.update(delta) //update animacji
            for (let i = 0; i < settings.createdAllies.length; i++) {
                if (settings.createdAllies[i].mixer) {
                    settings.createdAllies[i].mixer.update(delta)//; console.log("a")
                }
            }

            var playerClickedVect = that.movement.getClickedVect()
            var playerDirectionVect = that.movement.getDirectionVect()
            var playerDistanceTo = that.playerCont.position.clone().distanceTo(playerClickedVect)
            if (settings.firstTurn == true) {
                playerDistanceTo = 0
            }
            if (playerDistanceTo > 5) {
                that.playerCont.translateOnAxis(playerDirectionVect, 4)
                if (!isMovingPlayer) {
                    playerModel.mixer.uncacheRoot(playerModel)
                    playerModel.setAnimation("run");
                    isMovingPlayer = true
                }
            }
            else {
                if (isMovingPlayer) {
                    playerModel.clearAnimation()
                    playerModel.setAnimation("stand");
                    isMovingPlayer = false
                }
            }
            for (let i = 0; i < settings.choosenAllies.length; i++) {
                settings.allyVectors[i] = that.playerCont.clone().position
                settings.allyDirectionVectors[i] = settings.allyVectors[i].clone().sub(settings.choosenAllies[i].position).normalize()
                var AllyDistanceTo = settings.choosenAllies[i].position.clone().distanceTo(settings.allyVectors[i])

                for (let j = 0; j < settings.createdAllies.length; j++) {
                    if (settings.choosenAllies[i].uuid == settings.createdAllies[j].container.uuid) {
                        if (AllyDistanceTo >= (80 * i + 80)) {
                            // console.log("moving", settings.createdAllies[j].container.uuid)
                            settings.choosenAllies[i].translateOnAxis(settings.allyDirectionVectors[i], 5)

                            if (!settings.isMovingAlly[j]) {
                                settings.createdAllies[j].clearAnimation()
                                settings.createdAllies[j].setAnimation("run");
                                settings.isMovingAlly[j] = true
                            }
                        }
                        else {
                            if (settings.isMovingAlly[j] && !isMovingPlayer) {
                                settings.createdAllies[j].clearAnimation()
                                settings.createdAllies[j].setAnimation("stand");
                                settings.isMovingAlly[j] = false
                            }
                        }
                    }
                    for (let j = 0; j < settings.choosenAllies[i].children.length; j++) {
                        if (i == 0) {
                            var angle = Math.atan2(
                                settings.choosenAllies[i].clone().position.x - that.playerCont.clone().position.x,
                                settings.choosenAllies[i].clone().position.z - that.playerCont.clone().position.z
                            )
                            settings.choosenAllies[i].children[j].rotation.y = angle - Math.PI / 2
                        }
                        else {
                            var angle = Math.atan2(
                                settings.choosenAllies[i].clone().position.x - settings.choosenAllies[i - 1].clone().position.x,
                                settings.choosenAllies[i].clone().position.z - settings.choosenAllies[i - 1].clone().position.z
                            )
                            settings.choosenAllies[i].children[j].rotation.y = angle - Math.PI / 2
                        }
                    }
                }
            }
            that.camera.position.x = that.playerCont.position.x
            that.camera.position.z = that.playerCont.position.z + 700
            that.camera.position.y = that.playerCont.position.y + 600
            that.camera.lookAt(that.playerCont.position)
            if (that.movement) {
                that.movement.ring.rotation.y += 0.03
                if (that.movement.ring.rotation.y > Math.PI * 2) {
                    that.movement.ring.rotation.y = 0
                }
            }
            requestAnimationFrame(render);
            renderer.render(that.scene, that.camera);
        }
        this.ui() //input range swiatla
    }

    ui() {
        console.log("ui dziala")
        $("#lightPower").on("input", function () {
            const wartosc = this.value
            settings.lightPower = this.value
            const { savedLights } = settings
            for (let i = 0; i < savedLights.length; i++) {
                savedLights[i].power = wartosc * Math.PI
            }
        })
        $("#lightHeight").on("input", function () {
            const wartosc = this.value
            const { savedLights } = settings
            const { savedLightLamps } = settings
            for (let i = 0; i < savedLights.length; i++) {
                savedLights[i].position.y = wartosc
                savedLightLamps[i].position.y = wartosc
            }
        })
    }
    getCamera() {
        return this.camera
    }

}