console.log("settings.js działa")
const radius = 160 //promien tworzenia hexa
const dlugoscBoku = 2 * radius / Math.sqrt(3)
const wysokoscBoku = 50
const settings = {
    radius: radius, //promien tworzenia hexa
    dlugoscBoku: dlugoscBoku, //dlugosc boku tworzenia hexa
    wysokoscBoku: wysokoscBoku, //wysokosc boku tworzenia hexa
    wallMaterialDefault: new THREE.MeshPhongMaterial({ //default materiał do ścian hexa
        map: new THREE.TextureLoader().load("js3D/player/models/allyTexture.png"),
        color: 0xffffff,
        specular: 0xffffff,
        shininess: 5,
        // castShadow: true
    }),
    wallGeometryDefault: new THREE.BoxGeometry(dlugoscBoku, wysokoscBoku, 5), //default geometry do ścian hexa
    wallGeometryDoors: new THREE.BoxGeometry(dlugoscBoku / 3, wysokoscBoku, 5), //default geometry do ścian drzwi hexa
    modelMaterial: new THREE.MeshBasicMaterial({
        map: new THREE.TextureLoader().load("js3D/player/models/PlayerTexture.png"),
        morphTargets: true // ta własność odpowiada za animację materiału modelu
    }),
    allyModelMaterial: new THREE.MeshBasicMaterial({
        map: new THREE.TextureLoader().load("js3D/player/models/allyTexture.png"),
        morphTargets: true // ta własność odpowiada za animację materiału modelu
    }),
    // wallGeometryDoorsOut: new THREE.BoxGeometry(dlugoscBoku / 3, 50, 10), //default geometry do ścian drzwi hexa
    lightHeight: 450,
    lightPower: 2,
    savedLights: [],
    savedLightLamps: [],
    playerStartPos: [],
    firstTurn: true,
    intersectObjects: [], //elementy wyłapywane przez raycast
    choosenAllies: [],
    createdAllies: [],
    isMovingAlly: [],
    allyVectors: [],
    allyDirectionVectors: [],
    loadedModels: null,
    lastHoveredAlly: undefined,
}