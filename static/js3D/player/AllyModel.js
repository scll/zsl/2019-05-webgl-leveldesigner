class AllyModel3D {

    constructor() {
        this.container = new THREE.Object3D()
        this.mixer = null
    }
    loadModel(url, callback) {

        var loader = new THREE.JSONLoader();
        let allyModel2 = settings.createdAllies[settings.createdAllies.length - 1]
        // console.log("LOADMODEL11", allyModel2)
        loader.load(url, function (geometry) {
            // console.log("LOADMODEL22222", allyModel2)
            allyModel2.modelMesh = new THREE.Mesh(geometry, settings.allyModelMaterial)
            allyModel2.modelMesh.scale.set(2, 2, 2); //skala modelu
            // var box = new THREE.Box3().setFromObject(allyModel.modelMesh);
            allyModel2.modelMesh.position.y = wysokoscBoku / 2 + 2;
            allyModel2.modelMesh.rotation.y = Math.PI / 2
            // console.log(allyModel2.mixer)
            allyModel2.mixer = new THREE.AnimationMixer(allyModel2.modelMesh);
            allyModel2.mixer.clipAction("stand").play();
            // console.log(allyModel2.mixer)
            allyModel2.container.add(allyModel2.modelMesh)
            allyModel2.container.name = "ally"
            // const axis2 = new THREE.AxesHelper(200)
            // allyModel2.container.add(axis2)
            callback(allyModel2.container);
        });
    }
    updateModel(delta) {
        if (this.mixer) this.mixer.update(delta)
    }
    setAnimation(animation) {
        this.mixer.clipAction(animation).play();
    }
    clearAnimation() {
        if (this.mixer) { this.mixer.uncacheRoot(this.modelMesh) }
    }
}