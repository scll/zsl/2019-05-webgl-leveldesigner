class AllyRing {

    constructor() {
        console.log("AllyRing działa")
        this.container = new THREE.Object3D() // kontener na obiekty 3D
        // this.light = new THREE.SpotLight(0xff8800, 1, 70, (Math.PI), 0);
        // this.container.add(this.light)
        const material = new THREE.MeshBasicMaterial({ color: 0xff6600 })
        const geometry = new THREE.BoxGeometry(56, 1, 8)
        let wall = new THREE.Mesh(geometry, material);
        for (let n = 0; n < 6; n++) {
            let side = wall.clone()
            side.position.z = 45 * Math.cos(Math.PI / 3 * n)
            side.position.x = 45 * Math.sin(Math.PI / 3 * n)
            side.lookAt(this.container.position)
            this.container.add(side)
        }
        this.container.position.x = settings.playerStartPos[0]
        this.container.position.y = -settings.wysokoscBoku - 20
        this.container.position.z = settings.playerStartPos[1]
        return this.container
    }
}