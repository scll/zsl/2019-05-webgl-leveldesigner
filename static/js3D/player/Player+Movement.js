class Movement {

    constructor(player) {
        this.container = new THREE.Object3D()
        this.player = player //model gracza

        const { wysokoscBoku } = settings
        this.player.position.y = wysokoscBoku / 2 + 2
        this.player.rotation.y = Math.PI / 2
        this.container.add(this.player) // kontener w którym jest player

        this.light = new THREE.SpotLight(0xffffff, 5, 300, (Math.PI), 1, 2);
        this.light.position.set(0, 200, 0);
        this.container.add(this.light)

        this.axes = new THREE.AxesHelper(200)
        this.container.add(this.axes)

        this.raycaster = new THREE.Raycaster()
        this.mouseVector = new THREE.Vector2()
        this.clickedVect = new THREE.Vector3(0, 0, 0)
        this.directionVect = new THREE.Vector3(0, 0, 0)
        this.clickedDistance = 0
        const that = this
        $(document).contextmenu(function (e) {
            e.preventDefault();
        })
        $(document).mousedown(function () {
            that.move(event)
            const that2 = that
            // $(document).on("mousemove", function (event) {
            //     that2.move(event)
            // })
            // $(document).on("mouseup", function () {
            //     $(document).off("mousemove")
            // })
        })


        this.ring = new AllyRing()
        main.scene.add(this.ring)
        this.ringRaycaster = new THREE.Raycaster()
        this.ringMouseVector = new THREE.Vector2()
        $(document).on("mousemove", function (event) {
            that.ringF(event)
        })

    }
    move(event) {
        // console.log(event.button)
        const camera = main.getCamera()
        this.mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
        this.mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
        this.raycaster.setFromCamera(this.mouseVector, camera);
        var intersects = this.raycaster.intersectObjects(settings.intersectObjects, true); //true - mozliwosc "klikania" dzieci dzieci sceny
        if (intersects.length > 0) {
            // console.log("klik", intersects[0].object)
            if (intersects[0].object.name == "floor") {
                if (settings.firstTurn) settings.firstTurn = false
                this.clickedVect = intersects[0].point
                this.clickedVect.y = 0
                this.directionVect = this.clickedVect.clone().sub(this.container.position).normalize()
                var angle = Math.atan2(
                    this.container.position.clone().x - this.clickedVect.x,
                    this.container.position.clone().z - this.clickedVect.z
                )
                this.player.rotation.y = angle - Math.PI / 2
                this.axes.rotation.y = angle - Math.PI

                for (let i = 0; i < settings.choosenAllies.length; i++) {
                    settings.allyVectors[i] = this.container.clone().position
                    settings.allyDirectionVectors[i] = settings.allyVectors[i].clone().sub(settings.choosenAllies[i].position).normalize()
                }
            }
            // if (intersects[0].object.name == "ally") {
            //     console.log("ALLY")
            //     if (!settings.choosenAllies.includes(intersects[0].object)) {
            //         settings.choosenAllies.push(intersects[0].object)
            //         settings.allyVectors.push(new THREE.Vector3(0, 0, 0))
            //         settings.allyDirectionVectors.push(new THREE.Vector3(0, 0, 0))
            //     }
            // }
            if (intersects[0].object.parent.name == "ally") {
                intersects[0].object.parent.name = "choosenAlly"
                console.log("ALLY")
                if (!settings.choosenAllies.includes(intersects[0].object.parent)) {
                    settings.choosenAllies.push(intersects[0].object.parent)
                    settings.allyVectors.push(new THREE.Vector3(0, 0, 0))
                    settings.allyDirectionVectors.push(new THREE.Vector3(0, 0, 0))
                }
            }
        }
    }
    ringF(event) {
        const camera = main.getCamera()
        this.ringMouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
        this.ringMouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
        this.ringRaycaster.setFromCamera(this.ringMouseVector, camera);
        var intersects = this.ringRaycaster.intersectObjects(settings.intersectObjects, true);
        if (intersects.length > 0) {
            if (intersects[0].object.parent.name == "ally") {
                settings.lastHoveredAlly = intersects[0].object.parent
                this.ring.position.y = settings.lastHoveredAlly.position.y
                this.ring.position.x = settings.lastHoveredAlly.position.x
                this.ring.position.z = settings.lastHoveredAlly.position.z
                // this.ring.intensity = 2.5
            }
            if (intersects[0].object.parent.name != "ally") {
                if (settings.lastHoveredAlly) this.ring.position.y = settings.lastHoveredAlly.position.y - 50
                // this.ring.intensity = 0

            }
        }

    }
    getPlayerCont() {
        return this.container
    }
    getMouseVector() {
        return this.mouseVector
    }
    getClickedVect() {
        return this.clickedVect
    }
    getDirectionVect() {
        return this.directionVect
    }
}