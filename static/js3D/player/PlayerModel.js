class PlayerModel3D {

    constructor() {
        this.container = new THREE.Object3D()
        this.mixer = null
    }
    loadModel(url, callback) {
        var loader = new THREE.JSONLoader();
        loader.load(url, function (geometry) {

            playerModel.modelMesh = new THREE.Mesh(geometry, settings.modelMaterial)
            // playerModel.modelMesh.name = "name";
            playerModel.modelMesh.scale.set(2, 2, 2); //skala modelu
            // var box = new THREE.Box3().setFromObject(playerModel.modelMesh);
            playerModel.modelMesh.position.y = wysokoscBoku / 2 + 2;
            playerModel.mixer = new THREE.AnimationMixer(playerModel.modelMesh);
            playerModel.mixer.clipAction("stand").play();

            playerModel.container.add(playerModel.modelMesh)

            callback(playerModel.container);
        });
    }
    updateModel(delta) {
        if (this.mixer) this.mixer.update(delta)
    }
    setAnimation(animation) {
        this.mixer.clipAction(animation).play();
    }
    clearAnimation() {
        if (this.mixer) { this.mixer.uncacheRoot(this.modelMesh) }
    }
}