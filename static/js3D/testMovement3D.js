class testMovement3D {

    constructor() {

        const scene = new THREE.Scene();
        const windowWidth = $(window).width()
        const windowHeight = $(window).height()
        const camera = new THREE.PerspectiveCamera(
            45,    // kąt patrzenia kamery (FOV)
            windowWidth / windowHeight,    // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
            0.1,    // minimalna renderowana odległość
            10000    // maxymalna renderowana odległość od kamery
        );
        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor(0xffffff);
        renderer.setSize(windowWidth, windowHeight);
        $("#root").append(renderer.domElement);

        camera.position.set(0, 700, 700)
        camera.lookAt(scene.position)
        const axes = new THREE.AxesHelper(1000)
        scene.add(axes)

        var container = new THREE.Object3D() // kontener na obiekty 3D
        var floorGeo = new THREE.BoxGeometry(1000, 1, 1000, 30, 30, 30)
        var floorMaterial = new THREE.MeshBasicMaterial({
            transparent: false,
            wireframe: true,
            color: 0x000000,
            side: THREE.DoubleSide
        })
        var floor = new THREE.Mesh(floorGeo, floorMaterial)
        container.add(floor)
        scene.add(container)

        // player.position.y = 1
        // scene.add(container)
        const container2 = new THREE.Object3D()
        const player = new Enemy3D(); // player sześcian
        const { wysokoscBoku } = settings
        player.position.y = wysokoscBoku / 2 + 2
        container2.add(player) // kontener w którym jest player

        const axis2 = new THREE.AxesHelper(200) // osie konieczne do kontroli kierunku ruchu

        container2.add(axis2)
        scene.add(container2)

        // obj.intersObjects.push(floor)
        // main.raycast(scene, camera)

        $(document).mousedown(function () {
            move(event)
            $(document).on("mousemove", function (event) {
                move(event)
            })
            $(document).on("mouseup", function (event) {
                $(document).off("mousemove")
            })
        })
        var obj = {
            intersObjects: [],
            raycaster: new THREE.Raycaster(),
            mouseVector: new THREE.Vector2(),
            clickedVect: new THREE.Vector3(0, 0, 0),
            directionVect: new THREE.Vector3(0, 0, 0),
            clickedDistance: 0,
        }
        function move(event) {
            obj.mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
            obj.mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
            obj.raycaster.setFromCamera(obj.mouseVector, camera);
            var intersects = obj.raycaster.intersectObjects([floor], true); //true - mozliwosc "klikania" dzieci dzieci sceny
            if (intersects.length > 0) {
                obj.clickedVect = intersects[0].point
                obj.directionVect = obj.clickedVect.clone().sub(container2.position).normalize()
                obj.clickedDistance = container2.position.clone().distanceTo(obj.clickedVect)
                console.log(obj.clickedDistance)
                var angle = Math.atan2(
                    container2.position.clone().x - obj.clickedVect.x,
                    container2.position.clone().z - obj.clickedVect.z
                )
                player.rotation.y = angle - Math.PI
                axis2.rotation.y = angle - Math.PI
            }
        }
        function render() {
            var distanceTo = container2.position.clone().distanceTo(obj.clickedVect)
            if (distanceTo > 5) {
                container2.translateOnAxis(obj.directionVect, 5)
            }
            console.log(obj.clickedDistance)

            camera.position.x = container2.position.x
            camera.position.z = container2.position.z + 600
            camera.position.y = container2.position.y + 600
            camera.lookAt(container2.position)
            requestAnimationFrame(render);
            renderer.render(scene, camera);
        }
        render();
    }
}