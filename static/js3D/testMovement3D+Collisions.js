class testMovementCollisions3D {

    constructor() {

        var obj = {
            walls: [],
            intersObjects: [],
            raycaster: new THREE.Raycaster(),
            raycaster2: new THREE.Raycaster(),
            mouseVector: new THREE.Vector2(),
            clickedVect: new THREE.Vector3(0, 0, 0),
            directionVect: new THREE.Vector3(0, 0, 0),
            clickedDistance: 0,
        }

        const scene = new THREE.Scene();
        const windowWidth = $(window).width()
        const windowHeight = $(window).height()
        const camera = new THREE.PerspectiveCamera(
            45,    // kąt patrzenia kamery (FOV)
            windowWidth / windowHeight,    // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
            0.1,    // minimalna renderowana odległość
            10000    // maxymalna renderowana odległość od kamery
        );
        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor(0xffffff);
        renderer.setSize(windowWidth, windowHeight);
        $("#root").append(renderer.domElement);

        camera.position.set(0, 700, 700)
        camera.lookAt(scene.position)
        const axes = new THREE.AxesHelper(1000)
        scene.add(axes)

        var container = new THREE.Object3D() // kontener na obiekty 3D
        var floorGeo = new THREE.BoxGeometry(1000, 1, 1000, 30, 30, 30)
        var floorMaterial = new THREE.MeshBasicMaterial({
            transparent: false,
            wireframe: true,
            color: 0x000000,
            side: THREE.DoubleSide
        })
        var floor = new THREE.Mesh(floorGeo, floorMaterial)
        container.add(floor)
        scene.add(container)

        // player.position.y = 1
        // scene.add(container)
        const container2 = new THREE.Object3D()
        const player = new Enemy3D(); // player sześcian
        const { wysokoscBoku } = settings
        player.position.y = wysokoscBoku / 2 + 2
        container2.add(player) // kontener w którym jest player

        const axis2 = new THREE.AxesHelper(200)

        container2.add(axis2)
        scene.add(container2)

        const container3 = new THREE.Object3D() // kontener na obiekty 3D
        // this.light = new THREE.SpotLight(0xff8800, 1, 70, (Math.PI), 0);
        // this.container.add(this.light)
        const material = new THREE.MeshNormalMaterial()
        const wysokosc1 = 60
        const geometry = new THREE.BoxGeometry(256, wysokosc1, 8)
        let wall = new THREE.Mesh(geometry, material);
        for (let n = 0; n < 6; n++) {
            if (n != 4) {
                let side = wall.clone()
                side.position.z = 222 * Math.cos(Math.PI / 3 * n)
                side.position.x = 222 * Math.sin(Math.PI / 3 * n)
                side.lookAt(container3.position)
                container3.add(side)
                obj.walls.push(side)
            }
        }
        container3.position.x = 0
        container3.position.y = wysokosc1 / 2
        container3.position.z = 0

        scene.add(container3)
        // obj.intersObjects.push(floor)
        // main.raycast(scene, camera)

        var geometryS = new THREE.SphereGeometry(5, 32, 32);
        var materialS = new THREE.MeshBasicMaterial({ color: 0xffff00 });
        var sphere = new THREE.Mesh(geometryS, materialS);
        scene.add(sphere);


        $(document).mousedown(function () {
            move(event)
            $(document).on("mousemove", function (event) {
                move(event)
            })
            $(document).on("mouseup", function (event) {
                $(document).off("mousemove")
            })
        })

        function move(event) {
            obj.mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
            obj.mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
            obj.raycaster.setFromCamera(obj.mouseVector, camera);
            var intersects = obj.raycaster.intersectObjects([floor], true); //true - mozliwosc "klikania" dzieci dzieci sceny
            if (intersects.length > 0) {
                obj.clickedVect = intersects[0].point
                obj.directionVect = obj.clickedVect.clone().sub(container2.position).normalize()
                obj.clickedDistance = container2.position.clone().distanceTo(obj.clickedVect)
                console.log(obj.clickedDistance)
                var angle = Math.atan2(
                    container2.position.clone().x - obj.clickedVect.x,
                    container2.position.clone().z - obj.clickedVect.z
                )
                player.rotation.y = angle - Math.PI
                axis2.rotation.y = angle - Math.PI
            }
        }
        function collision() {
            var ray = new THREE.Ray(player.position, player.getWorldDirection(new THREE.Vector3(0, 0, 1)))
            obj.raycaster2.ray = ray
            var intersects2 = obj.raycaster2.intersectObjects(obj.walls, true);
            if (intersects2[0]) {
                // console.log(intersects2[0].distance) // odległość od vertex-a na wprost, zgodnie z kierunkiem ruchu
                console.log(intersects2[0].point) // współrzędne vertexa na wprost
                sphere.position.x = intersects2[0].point.x
                sphere.position.y = intersects2[0].point.y
                sphere.position.z = intersects2[0].point.z
            }

        }
        function render() {
            var distanceTo = container2.position.clone().distanceTo(obj.clickedVect)
            if (distanceTo > 5) {
                collision()
                container2.translateOnAxis(obj.directionVect, 5)
            }
            // console.log(obj.clickedDistance)

            camera.position.x = container2.position.x
            camera.position.z = container2.position.z + 600
            camera.position.y = container2.position.y + 600
            camera.lookAt(container2.position)
            requestAnimationFrame(render);
            renderer.render(scene, camera);
        }
        render();
    }
}