console.log("ui.js działa")

class UI {

    constructor() {
        this.load(3)
        this.sel1()
        this.updateJSON()
    }
    sel1() {
        var that = this
        $("#sel1").on("change", function () {
            $(".activeBt").removeClass("activeBt")
            $("#walls").addClass("activeBt")

            var ilosc = $("#sel1").val()
            that.load(ilosc)
            object1.newID = 0
            object1.listaID = []
            object1.tabData = {
                size: ilosc,
                level: [],
            }
            object1.currentType = "walls"
            ui.updateJSON()
        })
    }
    load(ilosc1, level1, obj) {
        // if (obj) document.getElementById("object1").innerHTML = obj.replace(/,"/g, ',</br>"').replace(/"}/g, '"</br>}</br>').replace(/{/g, '{</br>').replace(/,{/g, '{').replace(/\[/g, '').replace(/\]/g, '')
        function isOdd(num) { return num % 2; }
        $("#sel1").val(ilosc1)
        $("#plansza").empty()
        object1.newID = 0

        var ilosc2 = $("#sel1").val()

        for (var i = 0; i < ilosc2; i++) {
            for (var j = 0; j < ilosc2; j++) {
                var hex = document.createElement("div")
                hex.classList.add("hex")
                hex.id = "n" + j + "n" + i
                var id = hex.id
                if (level1) {
                    // console.log(level)
                    object1.tabData = {
                        size: ilosc2,
                        level: [],
                    }
                    var splitted = id.split("n")
                    for (var l = 0; l < level1.length; l++) {
                        // console.log(level1[l])
                        object1.tabData.level.push(level1[l])
                    }
                    // console.log(object1.tabData.level)
                    for (var k = 0; k < level1.length; k++) {
                        // console.log(splitted)
                        // console.log(level[k].x + " : " + level[k].z)
                        if (splitted[1] == level1[k].x && splitted[2] == level1[k].z) {
                            // hex.classList.add("selected", "sel" + level[k].dirOut)
                            hex.innerHTML = "<div class='selected sel" + level1[k].dirOut + "'>" + level1[k].dirOut + "</div>"
                            hex.id = level1[k].id
                            hex.className = "hex " + level1[k].type
                            object1.newID++
                        }
                    }
                }
                if (isOdd(j) == 0) {
                    // hex.classList.add("odd")
                    hex.style.top = 100 * i + "px"
                    hex.style.left = ((100 * j) - 10 * j) + "px"
                }
                else {
                    // hex.classList.add("even")
                    hex.style.top = ((100 * i) + 50) + "px"
                    hex.style.left = ((100 * j) - 10 * j) + "px"
                }
                $("#plansza").append(hex)
            }
        }
        $(".hex").click(this.sendData)
    }
    sendData() {
        var newID = object1.newID
        var obj = {}
        //poczatkowe divy mają "n" w id, po pierwszym kliknieciu zmieniają się na samą liczbę
        if (this.id[0] == "n") { //jesli dodajemy nowy hexagon
            object1.listaID.push(this.id)
            var splittedID = this.id.split("n")
            obj.id = newID
            obj.x = splittedID[1]
            obj.z = splittedID[2]
            obj.dirOut = 0
            obj.dirIn = 3
            obj.type = object1.currentType
            this.id = object1.newID
            this.classList = "hex " + obj.type
            this.innerHTML = "<div class='selected sel0'>0</div>"
            object1.newID++
            object1.tabData.level.push(obj)
            // console.log(obj)
        }
        else { //jesli nadpisujemy istniejący hexagon
            object1.tabData.level[this.id].type = object1.currentType //zmiana typu

            var dirOut = object1.tabData.level[this.id].dirOut
            dirOut < 5 ? object1.tabData.level[this.id].dirOut++ : object1.tabData.level[this.id].dirOut = 0;//zmiana dirOut

            var dirOut = object1.tabData.level[this.id].dirOut
            dirOut - 3 < 0 ? object1.tabData.level[this.id].dirIn = dirOut + 3 : object1.tabData.level[this.id].dirIn = dirOut - 3 //zmiana dirIn

            //update wizualny hexagonu:
            this.innerHTML = "<div class='selected sel" + object1.tabData.level[this.id].dirOut + "'>" + object1.tabData.level[this.id].dirOut + "</div>"
            this.classList = "hex " + object1.currentType

            // console.log(object1.tabData.level[this.id])
        }
        ui.updateJSON()
    }
    updateJSON() {
        document.getElementById("object1").innerHTML = JSON.stringify(object1.tabData, undefined, 2)
    }
}
